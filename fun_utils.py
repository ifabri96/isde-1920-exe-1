
import numpy as np
import matplotlib.pyplot as plt
from pandas import read_csv


def load_mnist(csv_filename = 'sample_data/mnist_test.csv'):
  """Load the MNIST digits from the notebook sample data (see "Files")"""
  mnist = read_csv(csv_filename)
  mnist = np.array(mnist)
  y = mnist[:,0]  # get the labels from the first column
  x = mnist[:, 1:]  # get the data from the other columns
  return x,y

def split_data(x,y,fract_tr=0.5):
  """Splits x,y into training and testing sets."""
  num_samples = y.size  # number of samples in data, equal to x.shape[0]
  n_tr = int(fract_tr * num_samples)
  n_ts = num_samples-n_tr
  # create a vector of indexes (ordered integer vector): [0, 1, 2, ...]
  idx = np.arange(num_samples)
  # shuffle it
  np.random.shuffle(idx)
  # extract training/testing idx from the shuffled idx vector
  idx_tr = idx[:n_tr]
  idx_ts = idx[n_tr:]
  assert(n_ts == idx_ts.size)  # assert is useful in unit testing
  # 5) extract training/testing samples from x (along with their labels y)
  x_tr = x[idx_tr, :]
  y_tr = y[idx_tr]
  x_ts = x[idx_ts, :]
  y_ts = y[idx_ts]
  return x_tr, x_ts, y_tr, y_ts



class NMC(object):

    def __init__(self):
        self._centroids = None

    @property
    def centroids(self):
        return self._centroids

    @centroids.setter
    def centroids(self, value):
        self._centroids = value

    def fit(self, x, y):
	num_classes = np.unique(y).size
  	num_features = x.shape[1]  # x.shape => (9999, 784)
  	centroids = np.zeros(shape=(num_classes, num_features))  # (10, 784)
  	for k in range(num_classes):
    		centroids[k, :] = x[y==k, :].mean(axis=0)
	return centroids			

        
    def predict(self, x):
        dist = euclidean_distances(x, centroids, squared=True)
    	y_pred = np.argmin(dist, axis=1)
    	return y_pred


